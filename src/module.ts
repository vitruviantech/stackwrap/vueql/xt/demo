import { defineNuxtModule, addPlugin, createResolver, addComponentsDir, addImportsDir } from '@nuxt/kit'

// Module options TypeScript interface definition
export interface ModuleOptions {}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: '@stackwrap/vueql-example-module',
    configKey: 'example'
  },
  // Default configuration options of the Nuxt module
  defaults: {},
  async setup (options, nuxt) {
    const resolver = createResolver(import.meta.url)

    // Do not add the extension since the `.ts` will be transpiled to `.mjs` after `npm run prepack`
    addPlugin(resolver.resolve('./runtime/plugin'))
    
    await addComponentsDir({
      path: resolver.resolve('./runtime/components'),
      prefix: 'Example',
      // pattern: "**/*.vue",
      // pathPrefix: false,
      // transpile: 'auto',
      // global: true
    })

    await addImportsDir(resolver.resolve('./runtime/composables'))
  }
})
